public enum Tile{
	BLANK("_"),
	HIDDEN_WALL("_"),
    WALL("W"),
	CASTLE("C");

//	public static void main(String[] args){
//		Tile one = Tile.BLANK;
//		System.out.println(one.getName());
//	}

	private final String name;

	private Tile(String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}
}