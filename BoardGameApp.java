import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
//		Tile trai = Tile.CASTLE;
//		System.out.println(trai.getName());

//		Board trai = new Board();
//		System.out.println("Base board");
//		System.out.println(trai);
//
//		Scanner r = new Scanner(System.in);
//		System.out.println("Input row and column");
//		int row = r.nextInt();
//		int col = r.nextInt();
//
//		trai.placeToken(row, col);
//		System.out.println(trai);
		Scanner r = new Scanner(System.in);
		Board game = new Board();
		int numCastles = 5;
		int lives = 4;
		System.out.println("Welcome to Castles and Walls. \n In this game You have " + numCastles +" Castles that you want to place, but you can't place them in top of walls. \n All walls are hidden at the start, and each time you place a castle on a hidden wall you lose a life. \n You have " + lives + " lives. Good luck!");
		
		while(numCastles>0&&lives>0){
			System.out.println(game);
			System.out.println("Number of Castles Left: " + numCastles);
			System.out.println("Number of Lives Left: "+lives);
			System.out.println("\n Input two integers for row and column to place your castle \n");
			int row = r.nextInt();
			int col = r.nextInt();
			int sol = game.placeToken(row,col);
			if (sol<0){
				System.out.println("You should already know you can't place a castle there. Try again.");
			} else if (sol == 1){
				System.out.println("There was a wall there. You lose one life.");
				lives-=1;
			} else if (sol == 0){
				System.out.println("You successfully placed one castle!");
				numCastles-=1;
			}
			//System.out.println("\033[H\033[2J"); tried to clear screen
		}
		
		if (numCastles==0){
			System.out.println("You Won!");
		} else{
			System.out.println("You Lost.");
		}
	}
}