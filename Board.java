import java.util.Random;
public class Board{
	Tile[][] grid;

	public Board(){
		this.grid = new Tile[5][5];

		for (int i = 0; i<5; i++){
			for (int j = 0; j<5; j++){
				grid[i][j] = Tile.BLANK;
			}
		}
		
		Random rng = new Random();
		
		for (int i = 0; i<5; i++){
			grid[i][rng.nextInt(5)] = Tile.HIDDEN_WALL;
		}
	}
	
	public String toString(){
		String p = "";

		for (int i = 0; i<5; i++){
			for (int j = 0; j<5; j++){
				if (j==4){
					p += grid[i][j].getName();
				} else{
					p += grid[i][j].getName() + " ";
				}
			}
			p += "\n";
		}

		return p;
	}

	public int placeToken(int row, int col){
		if (row>5 || row<1 || col>5 || col<1){
			return -2;
		} else if(grid[row-1][col-1] == Tile.CASTLE || grid[row-1][col-1] == Tile.WALL){
			return -1;
		} else if(grid[row-1][col-1] == Tile.HIDDEN_WALL){
			grid[row-1][col-1] = Tile.WALL;
			return 1;
		} else{
			grid[row-1][col-1] = Tile.CASTLE;
			return 0;
		}
	}

//		public checkIfFull(){
//			for (int i = 0; i<3; i++){
//				for (int j = 0; j<3; j++){
//					if (grid[i][j] == Tile.BLANK){
//						return false;
//					}
//				}
//			}
//			return true;
//		}
//
//		private checkIfWinningHorizontal(Tile playerToken){
//			boolean check = true;
//
//			for (int i = 0; i<3; i++){
//				for (int j=0;j<3;j++){
//					if(grid[i][j]!=playerToken){
//						check = false;
//					}
//				}
//				if (check){
//					return check;
//				} else if(i<2){
//					check = true;
//				}
//			}
//
//			return false;
//		}
//
//		private checkIfWinningVertical(Tile playerToken){
//			boolean check = true;
//
//			for (int i = 0; i<3; i++){
//				for (int j=0;j<3;j++){
//					if(grid[j][i]!=playerToken){
//						check = false;
//					}
//				}
//				if (check){
//					return check;
//				} else if(i<2){
//					check = true;
//				}
//			}
//
//			return false;
//		}
//
//		public checkIfWinning(Tile playerToken){
//			return checkIfWinningHorizontal(placeToken) || checkIfWinningVertical(playerToken);
//		}
}